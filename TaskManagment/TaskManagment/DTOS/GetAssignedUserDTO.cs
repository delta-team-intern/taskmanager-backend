﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskManagment.DTOS
{
    public class GetAssignedUserDTO
    {
        public int AssignedUserId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}
